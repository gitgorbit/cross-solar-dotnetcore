﻿using CrossSolar.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Newtonsoft.Json.Linq;

namespace CrossSolar.Tests.Exceptions
{
    public class HttpStatusCodeExceptionTests
    {

        [Fact]
        public void HttpStatusCodeExceptionTests_ConstructorStatusCode_ShouldSetStatusCode()
        {
            //Arrange
            var actual = 12;

            // Act
            var httpStatusCodeException = new HttpStatusCodeException(actual);

            //Assert
            Assert.Equal("text/plain", httpStatusCodeException.ContentType);
            Assert.Equal(httpStatusCodeException.StatusCode, actual);
        }


        [Fact]
        public void HttpStatusCodeExceptionTests_ConstructorStatusCodeMessage_ShouldSetStatusCode()
        {
            //Arrange
            var actual = 12;

            // Act
            var httpStatusCodeException = new HttpStatusCodeException(actual, "message");

            //Assert
            Assert.Equal("text/plain", httpStatusCodeException.ContentType);
            Assert.Equal(httpStatusCodeException.StatusCode, actual);
        }

        [Fact]
        public void HttpStatusCodeExceptionTests_ConstructorStatusException_ShouldSetStatusCode()
        {
            //Arrange
            var actual = 12;
            var exception = new Exception();
            // Act
            var httpStatusCodeException = new HttpStatusCodeException(actual, exception);

            //Assert
            Assert.Equal("text/plain", httpStatusCodeException.ContentType);
            Assert.Equal(httpStatusCodeException.StatusCode, actual);
        }

        [Fact]
        public void HttpStatusCodeExceptionTests_ConstructorStatusJObject_ShouldSetStatusCodeAndContentType()
        {
            //Arrange
            var actual = 12;
            var jObject = new JObject();
            // Act
            var httpStatusCodeException = new HttpStatusCodeException(actual, jObject);

            //Assert
            Assert.Equal("application/json", httpStatusCodeException.ContentType);
            Assert.Equal(httpStatusCodeException.StatusCode, actual);
        }
    }
}
