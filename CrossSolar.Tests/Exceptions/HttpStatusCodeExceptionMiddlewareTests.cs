﻿using Microsoft.Extensions.Logging;
using CrossSolar.Exceptions;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Microsoft.AspNetCore.Http;

namespace CrossSolar.Tests.Exceptions
{
    public class HttpStatusCodeExceptionMiddlewareTests
    {
        private readonly Mock<ILoggerFactory> _logerFactoryMock = new Mock<ILoggerFactory>();
        private readonly Mock<RequestDelegate> _requestDelegate = new Mock<RequestDelegate>();

        [Fact]
        public void HttpStatusCodeExceptionMiddlewareTests_ConstructorRequestDelegateIsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new HttpStatusCodeExceptionMiddleware(null, _logerFactoryMock.Object));
        }

        [Fact]
        public void HttpStatusCodeExceptionMiddlewareTests_ConstructorLoggerFactoryIsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new HttpStatusCodeExceptionMiddleware(_requestDelegate.Object, null));
        }

        [Fact]
        public async void Invoke_Should_CallNext()
        {
            var nextCalled = false;
            var logResponseMiddleware = new HttpStatusCodeExceptionMiddleware(next: async (innerHttpContext) =>
            {
                await innerHttpContext.Response.WriteAsync("text");
                nextCalled = true;
            }, loggerFactory: _logerFactoryMock.Object);


            var defaultHttpContext = new DefaultHttpContext();
            await logResponseMiddleware.Invoke(defaultHttpContext);

            Assert.True(nextCalled);
        }
    }
}
