﻿using CrossSolar.Controllers;
using CrossSolar.Domain;
using CrossSolar.Models;
using CrossSolar.Repository;
using CrossSolar.Tests.AsyncDbSetup;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace CrossSolar.Tests.Controller
{
    public class AnalyticsControllerTests
    {
        public AnalyticsControllerTests()
        {
            _analyticsController = new AnalyticsController(_analyticsRepositoryMock.Object, _panelRepositoryMock.Object);
        }

        private readonly AnalyticsController _analyticsController;
        private readonly Mock<IAnalyticsRepository> _analyticsRepositoryMock = new Mock<IAnalyticsRepository>();
        private readonly Mock<IPanelRepository> _panelRepositoryMock = new Mock<IPanelRepository>();

        [Fact]
        public async Task Get_ShouldReturn404_WhenPanelIsNull()
        {
            var panels = Enumerable.Empty<Panel>().ToAsyncDbSetMock();

            var contextOptions = new DbContextOptions<CrossSolarDbContext>();
            var mockContext = new Mock<CrossSolarDbContext>(contextOptions);
            mockContext.Setup(c => c.Set<Panel>()).Returns(panels.Object);

            var panelRepository = new PanelRepository(mockContext.Object);
            var analyticsController = new AnalyticsController(_analyticsRepositoryMock.Object, panelRepository);
            var result = await analyticsController.Get(It.IsAny<string>());

            var actual = (result as NotFoundResult).StatusCode;

            Assert.Equal(404, actual);
        }

        [Fact]
        public async Task Post_ShoudReturn400_WhenModelsInvalid()
        {
            //Arrange
            var viewModel = new OneHourElectricityModel();
            // Act
            _analyticsController.ModelState.AddModelError("Id", "Id");
            var result = await _analyticsController.Post("blah", viewModel);

            // Assert
            var badRequestResult = result as BadRequestObjectResult;
            Assert.Equal(400, badRequestResult.StatusCode);
        }

        [Fact]
        public async Task Post_ShoudInsertOneHourElectricity()
        {
            //Arrange
            var model = new OneHourElectricityModel();

            // Act
            var result = await _analyticsController.Post("blah", model);

            //verify method is called
            _analyticsRepositoryMock.Verify(x => x.InsertAsync(It.IsAny<OneHourElectricity>()), Times.Once);

            // Assert
            Assert.NotNull(result);
            var createdResult = result as CreatedResult;
            Assert.Equal(201, createdResult.StatusCode);
        }

        [Fact]
        public async Task DayResults_ShouldReturn404_WhenPanelIsNull()
        {
            //Arrange
            var panels = Enumerable.Empty<Panel>().ToAsyncDbSetMock();

            var contextOptions = new DbContextOptions<CrossSolarDbContext>();
            var mockContext = new Mock<CrossSolarDbContext>(contextOptions);
            mockContext.Setup(c => c.Set<Panel>()).Returns(panels.Object);

            var panelRepository = new PanelRepository(mockContext.Object);
            var analyticsController = new AnalyticsController(_analyticsRepositoryMock.Object, panelRepository);

            // Act
            var result = await analyticsController.DayResults(It.IsAny<string>());
            var actual = (result as NotFoundResult).StatusCode;

            // Assert
            Assert.Equal(404, actual);
        }

        [Fact]
        public async Task DayResults_ShouldReturnOk()
        {
            //Arrange
            var seriald = "XXXX1111YYYY2222";
            var panels = new List<Panel> {
                new Panel { Serial = seriald, Latitude = 123456d, Longitude = 123456d  }
            }.ToAsyncDbSetMock();

            var oneDayElectricityModels = new List<OneDayElectricityModel>().ToAsyncDbSetMock();
            var oneHourElectricity = new List<OneHourElectricity>().ToAsyncDbSetMock();

            var contextOptions = new DbContextOptions<CrossSolarDbContext>();
            var mockContext = new Mock<CrossSolarDbContext>(contextOptions);
            mockContext.Setup(c => c.Set<Panel>()).Returns(panels.Object);
            mockContext.Setup(c => c.Set<OneDayElectricityModel>()).Returns(oneDayElectricityModels.Object);
            mockContext.Setup(c => c.Set<OneHourElectricity>()).Returns(oneHourElectricity.Object);

            var panelRepository = new PanelRepository(mockContext.Object);
            var analyticRepository = new AnalyticsRepository(mockContext.Object);
            var analyticsController = new AnalyticsController(analyticRepository, panelRepository);

            // Act
            var actionResult = await analyticsController.DayResults(seriald);

            //Assert
            var actual = (actionResult as OkObjectResult).StatusCode;
            Assert.Equal(200, actual);
        }
    }
}
