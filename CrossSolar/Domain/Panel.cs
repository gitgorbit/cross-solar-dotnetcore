﻿using System.ComponentModel.DataAnnotations;

namespace CrossSolar.Domain
{
    public class Panel
    {
        public int Id { get; set; }

        [RegularExpression(@"^\d+(\.\d{6})$")]
        [Required]
        public double Latitude { get; set; }

        [RegularExpression(@"^\d+(\.\d{6})$")]
        public double Longitude { get; set; }

        [StringLength(16)]
        [Required]
        public string Serial { get; set; }

        public string Brand { get; set; }
    }
}